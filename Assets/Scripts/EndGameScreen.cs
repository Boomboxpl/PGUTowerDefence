﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameScreen : MonoBehaviour
{
    public Text waveText;

    [SerializeField] GameObject turretChoice = null;
    [SerializeField] GameObject playerStatus = null;

    void Start()
    {
        GameManager.Instance.gameScore.noLife += Show;
        gameObject.SetActive(false);
    }

    public void Replay()
    {
        gameObject.SetActive(false);
        turretChoice.SetActive(true);
        playerStatus.SetActive(true);
        GameManager.Instance.StartNewGame();
    }

    void Show()
    {
        gameObject.SetActive(true);
        turretChoice.SetActive(false);
        playerStatus.SetActive(false);
        waveText.text = $"Wave: {GameManager.Instance.gameScore.waveCount}";
    }

    void CheckUIElements() {
        if(turretChoice == null || playerStatus == null){
            Debug.LogError("TurretChoice or PlayerStatus are not assigned!");
        }
    }
}
