﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Enemy : MonoBehaviour
{
    public static List<Enemy> spawnedEnemies { get; private set; }

    public float velocity = 1f;
    public float life = 0.5f;

    public bool IsAlive => life > 0f;

    private int currentWaypoint = 0;
    private float damage = 0.1f;
    private float checkWPDistance = 0.2f;
    public float maxLife {get; private set; }
    [SerializeField] ParticleSystem enemyDeathEffect;

    static Enemy()
    {
        spawnedEnemies = new List<Enemy>();
    }

    void Awake()
    {
        if (spawnedEnemies == null)
        {
            spawnedEnemies = new List<Enemy>();
        }
        Debug.Log("LOOKAT awaken");
        spawnedEnemies.Add(this);

        GameManager.Instance.gameScore.noLife += OnPlayerNoLife;
        maxLife = life;
    }

    void Start() {
    }

    void Update()
    {
        int wpId = currentWaypoint + 1;
        if (wpId < EnemySpawner.enemyWalkingPath.Count)
        {
            Vector3 nextWP = EnemySpawner.enemyWalkingPath[wpId].position;
            Vector3 moveVector = nextWP - transform.position;
            Vector3 moveVectorNormalized = (moveVector).normalized;
            moveVectorNormalized.y = 0;
            transform.position += moveVectorNormalized * velocity * Time.deltaTime;

            Vector3 newDistance = moveVector;
            newDistance.y = 0;
            float distanceToWP = Vector3.Magnitude(newDistance);

            transform.rotation = Quaternion.LookRotation(moveVectorNormalized, Vector3.up);

            if (distanceToWP < checkWPDistance)
            {
                currentWaypoint++;
            }
        }
        else
        {
            //Health reduce
            score.life -= damage;
            int lifeLeftPercentage = (int)Math.Round(score.life * 100);
            Debug.Log("Enemy passed the route! " + lifeLeftPercentage + "% Life left");
            Destroy(gameObject);
        }
    }

    void OnDestroy()
    {
        if(GameManager.Instance?.gameScore != null)
        {
            GameManager.Instance.gameScore.noLife -= OnPlayerNoLife;
        }
        spawnedEnemies.Remove(this);
    }

    public void Hit(float damage)
    {
        Debug.Log("Hit " + damage);
        life -= damage;
        if (life <= 0)
        {            
            Instantiate(enemyDeathEffect, transform.position, Quaternion.identity);
            Debug.Log("EnemyKilled");
            Destroy(gameObject);
        }
    }

    void OnPlayerNoLife()
    {
        Destroy(gameObject);
    }

    GameScore score => GameManager.Instance.gameScore;
}
