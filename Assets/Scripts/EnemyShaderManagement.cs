﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShaderManagement : MonoBehaviour
{
    private Renderer renderer;
    private float intensity = 1.0f;
    private Shader shader;
    private Enemy enemyRef;


    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>();
        enemyRef = GetComponent<Enemy>();
        if(renderer && enemyRef) {
            string shaderName = "Custom/ScratchesEnemy";
            renderer.material.shader = Shader.Find(shaderName);
            shader = renderer.material.shader;
            float lifeNormalised = enemyRef.life/enemyRef.maxLife;
            intensity = 1 - lifeNormalised;
            renderer.material.SetFloat("_Scratches", intensity);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if(renderer && enemyRef) {
            float lifeNormalised = enemyRef.life/enemyRef.maxLife;
            intensity = 1 - lifeNormalised;
            renderer.material.SetFloat("_Scratches", intensity);
        }
    }
}
