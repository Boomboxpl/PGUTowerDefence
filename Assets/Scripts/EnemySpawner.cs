﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemySpawner : MonoBehaviour
{
    public Action waveEnded;

    public int enemiesInWave = 5;
    public Transform spawnPoint;
    public Enemy enemyPrefab;
    public Enemy specialEnemyPrefab;
    private int waveMultiplier = 1;

    public static List<Transform> enemyWalkingPath = null;

    public void SpawnWave()
    {
        InitializeComponents();
        StartCoroutine(SpawnCoroutine());
    }

    void InitializeComponents() {
        if (enemyWalkingPath == null) {
            enemyWalkingPath = new List<Transform>();
            GameObject pathWaypointsContainer = GameObject.FindGameObjectWithTag("PathWaypoints");
            if(!pathWaypointsContainer) {
                Debug.LogError("PathWaypointsContainer wasn't found!");
                return;
            }
            for(int i = 0; i < pathWaypointsContainer.transform.childCount; i++) {
                GameObject waypoint = pathWaypointsContainer.transform.GetChild(i).gameObject;
                enemyWalkingPath.Add(waypoint.transform);
            }            
        } 
    }

    IEnumerator SpawnCoroutine()
    {
        bool isSpecialEnemySpawned = false;
        enemiesInWave = enemiesInWave + waveMultiplier;
        for (int i = 0; i < enemiesInWave; i++)
        {
            Enemy newEnemy;
            if(i == enemiesInWave-1) {
                newEnemy = Instantiate(specialEnemyPrefab, transform);
                isSpecialEnemySpawned = true;
            }
            else {
                newEnemy = Instantiate(enemyPrefab, transform);
            }
            // newEnemy.transform.position = spawnPoint.position;
            Collider newEnemyCollider = newEnemy.GetComponent<Collider>();
            Vector3 spawnPos = enemyWalkingPath[0].position;
            float plusHeight = isSpecialEnemySpawned ? newEnemyCollider.bounds.size.y/4 : newEnemyCollider.bounds.size.y/2;
            newEnemy.transform.position =  spawnPos+ new Vector3(0, plusHeight, 0);
            yield return new WaitForSeconds(2f);
        }
        waveMultiplier++;
        yield return new WaitWhile(() => Enemy.spawnedEnemies.Count > 0f);
        waveEnded?.Invoke();
    }

}
