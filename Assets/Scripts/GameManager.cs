﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class GameManager : MonoBehaviour
{
    public MapManager mapManager;
    [HideInInspector] public GameScore gameScore;

    GameSaveManager gameSaveManager;
    EnemySpawner enemySpawner;
    [SerializeField][Range(0.1f,10.0f)] float timeScale = 1f;

    [SerializeField] private Slider timeScaleSlider;

    public bool isInEdit { get; private set; }

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
            }

            return _instance;
        }
    }
    static GameManager _instance;

    void Awake()
    {
        isInEdit = true;
        gameScore = new GameScore();
        gameSaveManager = FindObjectOfType<GameSaveManager>();
        enemySpawner = FindObjectOfType<EnemySpawner>();
        enemySpawner.waveEnded += WaveEnded;
        if (!mapManager)
        {
            Debug.LogError("MapManager is not set!");
            return;
        }
        else
        {
            StartNewGame();
        }
        InitializeTimeScaleSlider();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartNewWave();
        }
        if(Input.GetKeyDown(KeyCode.Escape)) {
            Application.Quit();
        }        
    }

    public void StartNewWave()
    {
        if(isInEdit == true)
        {
            isInEdit = false;
            gameScore.waveCount++;
            enemySpawner.SpawnWave();
        }
    }

    public void StartNewGame()
    {
        isInEdit = true;
        Action previosuNoLifeActions = gameScore?.noLife;
        EnemySpawner.enemyWalkingPath = null;
        gameScore = new GameScore();
        gameScore.noLife = previosuNoLifeActions;
        mapManager.BuildMap();
        //mapManager.RandomSpawnTurrets();
    }

    void WaveEnded()
    {
        isInEdit = true;
        gameSaveManager.SaveGame();
    }

    public GameScore GetGameScore() {
        return gameScore;
    }

    private void InitializeTimeScaleSlider() {
        timeScaleSlider.minValue = 0.1f;
        timeScaleSlider.maxValue = 10.0f;
        timeScaleSlider.value = timeScale;
    }

    public void TimeScaleOnValueChanged() {
        timeScale = timeScaleSlider.value;        
        Time.timeScale = timeScale;
    }
}
