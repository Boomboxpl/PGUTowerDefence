﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class GameModeButton : MonoBehaviour
{
    public Sprite editModeSprite;
    public Sprite waveModeSprite;
    public Image icon;

    Button button;

    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(GameManager.Instance.StartNewWave);
    }

    void Update()
    {
        button.interactable = GameManager.Instance.isInEdit;
        Sprite newSprite = GetSprite();
        if(newSprite != icon.sprite)
        {
            icon.sprite = newSprite;
        }
    }

    Sprite GetSprite() => GameManager.Instance.isInEdit ? editModeSprite : waveModeSprite;
}
