﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class HealButton : MonoBehaviour
{
    public int moneyPerHeal = 200;
    public float healValue = 0.2f;

    public Text requirementText;

    Button button;

    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(Heal);
        if(requirementText != null)
        {
            requirementText.text = moneyPerHeal.ToString();
        }
    }

    void Update()
    {
        button.interactable = CanHeal;
    }

    public void Heal()
    {
        if(CanHeal)
        {
            score.life += healValue;
            score.money -= moneyPerHeal;
        }
    }

    bool CanHeal => score.life < 1f && score.money >= moneyPerHeal && GameManager.Instance.isInEdit;

    GameScore score => GameManager.Instance.gameScore;
}
