﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class MapManager : MonoBehaviour
{
    public MapNode nodePrefab;

    public MapConfiguration mapConfiguration;
    public TurretShooter turretTemplate;

    public List<MapNode> nodes { get; private set; }
    public List<MapNode> pathNodes {get; private set; }

    private GameObject pathWaypointsContainer;

    public Material PathMaterial;
    public Material StartMaterial;
    public Material EndMaterial;


    void InitializeComponents() {
        pathWaypointsContainer = GameObject.FindGameObjectWithTag("PathWaypoints");
        if(!pathWaypointsContainer) {
            Debug.LogError("PathWaypointsContainer not found!");
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.white;
        Vector3 realSize = new Vector3(mapConfiguration.mapSize.x * mapConfiguration.positionDelta, 1, -mapConfiguration.mapSize.y * mapConfiguration.positionDelta);
        float offset = 0.5f * mapConfiguration.positionDelta;
        Gizmos.DrawWireCube(realSize / 2f - new Vector3(offset, 0, -offset), realSize);
    }

    public void BuildMap()
    {
        InitializeComponents();
        ClearMap();

        for (int y = 0; y < mapConfiguration.mapSize.y; y++)
        {
            for (int x = 0; x < mapConfiguration.mapSize.x; x++)
            {
                MapNode newNode = Instantiate(nodePrefab, transform);
                newNode.positon = new Vector2Int(x, y);
                newNode.transform.position = new Vector3(x * mapConfiguration.positionDelta, 0f, -y * mapConfiguration.positionDelta);
                nodes.Add(newNode);
            }
        }

        SetEnemiesPath();
    }

    public void LoadConfiguration(MapConfiguration newMapConfiguration, List<TurretShooter.TurretConfiguration> turretConfigurations)
    {
        mapConfiguration = newMapConfiguration;
        BuildMap();
    }

    public MapNode GetNodeAt(Vector2Int mapPosition)
    {
        if (mapPosition.x >= mapConfiguration.mapSize.x || mapPosition.x < 0 || mapPosition.y >= mapConfiguration.mapSize.y || mapPosition.y < 0)
        {
            return null;
        }
        int linearPosition = mapPosition.y * mapConfiguration.mapSize.x + mapPosition.x;
        if (nodes.Count < linearPosition)
        {
            return null;
        }
        return nodes[linearPosition];
    }

    public List<TurretShooter.TurretConfiguration> GetTurretsConfigruations()
    {
        return nodes.Where(node => node.turretShooter != null).Select(node => node.turretShooter.configuration).ToList();
    }

    public List<MapNode> GetEmptyNodes()
    {
        return nodes.Where(node => node.turretShooter == null && !pathNodes.Contains(node)).ToList();
    }

    public void RandomSpawnTurrets()
    {
        List<MapNode> nodesToOcupy = GetRandomEmptyNodes();

        foreach (MapNode node in nodesToOcupy)
        {
            node.SetTurret(Instantiate(turretTemplate));
            node.turretShooter.PrepareConfigurationData(turretTemplate.name, node.positon);
        }
    }

    List<MapNode> GetRandomEmptyNodes()
    {
        List<MapNode> emptyNodes = GetEmptyNodes();
        List<MapNode> resultNodes = new List<MapNode>(mapConfiguration.nbOfTurrets);
        for (int i = 0; i < mapConfiguration.nbOfTurrets; i++)
        {
            int randID = Random.Range(0, emptyNodes.Count);
            resultNodes.Add(emptyNodes[randID]);
        }
        return resultNodes;
    }

    void ClearMap()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
        nodes = new List<MapNode>();
    }

    void SetEnemiesPath() {
        foreach(Transform child in pathWaypointsContainer.transform)
        {
            Destroy(child.gameObject);
        }

        (Vector2Int, Vector2Int) positionsTuple = GetStartAndEndPositions();

        MapNode startNode = GetNodeAt(positionsTuple.Item1);
        MapNode endNode = GetNodeAt(positionsTuple.Item2);
        Renderer sNrenderer = startNode.GetComponent<Renderer>();
        Renderer eNrenderer = endNode.GetComponent<Renderer>();
        if(!sNrenderer || !eNrenderer)
        {
            Debug.LogError("Renderer(s) not found!");
            return;
        }
        sNrenderer.material = StartMaterial;
        eNrenderer.material = EndMaterial;
        
        SetEnemyPaths(positionsTuple);

    }

    (Vector2Int, Vector2Int) GetStartAndEndPositions() {
        int mapSize = mapConfiguration.mapSize.x;
        if(mapSize != mapConfiguration.mapSize.y)
            Debug.LogWarning("Map is not a square!");

        //Setting positions on border where start point and end point can be placed
        List<Vector2Int> requiredPositions = new List<Vector2Int>();
        int borderArraySize = mapSize-1;
        for(int cord = 1; cord < borderArraySize; cord++) {
            requiredPositions.Add(new Vector2Int(cord, 0));                 //Upper nodes
            requiredPositions.Add(new Vector2Int(cord, borderArraySize));   //Bottom nodes
            requiredPositions.Add(new Vector2Int(0, cord));                 //Left nodes
            requiredPositions.Add(new Vector2Int(borderArraySize, cord));   //Right nodes
        }
        
        Vector2Int startPointPos = requiredPositions[Random.Range(0, requiredPositions.Count)];
        Vector2Int endPointPos;
        //Checking which border (top/bottom/left/right)
        int randEndPos = Random.Range(1, borderArraySize);
        switch(CheckBorderNameByPosition(startPointPos)) {
            case "Top":
                endPointPos = new Vector2Int(randEndPos, borderArraySize);
                break;
            case "Bottom":
                endPointPos = new Vector2Int(randEndPos, 0);
                break;
            case "Left":
                endPointPos = new Vector2Int(borderArraySize,  randEndPos);
                break;
            case "Right":
                endPointPos = new Vector2Int(0, randEndPos);
                break;
            default:
                Debug.LogError("UnknownBorderNameError!");
                return (new Vector2Int(-1,-1), new Vector2Int(-1,-1));
        }

        return (startPointPos, endPointPos);
    }

    void SetEnemyPaths((Vector2Int, Vector2Int) startAndEndPos) {
        int mapSize = mapConfiguration.mapSize.x;   //It is already checked if mapSize.x is equal to mapSize.y
        Vector2Int centerPointPos;
        if(mapSize%2 != 1) {
            List<Vector2Int> randomCenterPos = new List<Vector2Int>();
            int halfOfMapSize = mapSize/2;
            randomCenterPos.Add(new Vector2Int(halfOfMapSize,halfOfMapSize));       //Right bottom corner of center point
            randomCenterPos.Add(new Vector2Int(halfOfMapSize-1,halfOfMapSize-1));   //Left upper corner of center point
            randomCenterPos.Add(new Vector2Int(halfOfMapSize-1,halfOfMapSize));     //Left bottom corner of center point
            randomCenterPos.Add(new Vector2Int(halfOfMapSize,halfOfMapSize-1));     //Right upper corner of center point
            centerPointPos = randomCenterPos[Random.Range(0,randomCenterPos.Count)];    //Random selection of "center" point
        }
        else {
            int center = (mapSize-1) / 2;
            centerPointPos = new Vector2Int(center, center);
        }

        List<MapNode> enemyPathNodes = new List<MapNode>();
        List<MapNode> waypointsToAdd = new List<MapNode>();
        MapNode startNode = GetNodeAt(startAndEndPos.Item1);

        enemyPathNodes.Add(startNode);
        waypointsToAdd.Add(startNode);

        bool isXAxisFirst = (Random.value > 0.5f);
        Vector2Int curPos = startAndEndPos.Item1;
        Vector2Int endPos = startAndEndPos.Item2;

        //Getting nodes to the center
        if(isXAxisFirst) {
            GetPosToDestinationPoint(ref curPos, centerPointPos, enemyPathNodes, true);
            waypointsToAdd.Add(GetNodeAt(curPos));
            GetPosToDestinationPoint(ref curPos, centerPointPos, enemyPathNodes, false);
            waypointsToAdd.Add(GetNodeAt(curPos));
            GetPosToDestinationPoint(ref curPos, endPos, enemyPathNodes, true);
            waypointsToAdd.Add(GetNodeAt(curPos));
            GetPosToDestinationPoint(ref curPos, endPos, enemyPathNodes, false);
            waypointsToAdd.Add(GetNodeAt(curPos));
        }
        else {
            GetPosToDestinationPoint(ref curPos, centerPointPos, enemyPathNodes, false);
            waypointsToAdd.Add(GetNodeAt(curPos));
            GetPosToDestinationPoint(ref curPos, centerPointPos, enemyPathNodes, true);
            waypointsToAdd.Add(GetNodeAt(curPos));
            GetPosToDestinationPoint(ref curPos, endPos, enemyPathNodes, false);
            waypointsToAdd.Add(GetNodeAt(curPos));
            GetPosToDestinationPoint(ref curPos, endPos, enemyPathNodes, true);
            waypointsToAdd.Add(GetNodeAt(curPos));
        }

        foreach (MapNode node in waypointsToAdd)
        {
            AddPathWaypoint(node);
        }

        Debug.Log("HowManyPathNodes: " + enemyPathNodes.Count);
        Debug.Log("HowManyWaypoints: " + waypointsToAdd.Count);
        foreach (MapNode node in enemyPathNodes)
        {
            if(node.positon != startAndEndPos.Item1 && node.positon != startAndEndPos.Item2) {
                Renderer nodeRenderer = node.GetComponent<Renderer>();
                if(!nodeRenderer)
                {
                    Debug.LogError("Renderer(s) not found!");
                    return;
                }
                nodeRenderer.material = PathMaterial;
            }
        }
        pathNodes = enemyPathNodes;
    }

    void GetPosToDestinationPoint(ref Vector2Int curPos, Vector2Int desPos, List<MapNode> enemyPathNodes, bool isXAxisPosWanted) {
        int desAxisPos = isXAxisPosWanted ? desPos.x : desPos.y;
        int curAxisPos = isXAxisPosWanted ? curPos.x : curPos.y;
        while (curAxisPos != desAxisPos) {
            curAxisPos = curAxisPos < desAxisPos ? curAxisPos+1 : curAxisPos-1;
            if (isXAxisPosWanted)
                curPos.x = curAxisPos;
            else
                curPos.y = curAxisPos;
            enemyPathNodes.Add(GetNodeAt(curPos));
        }
    }

    void AddPathWaypoint(MapNode node) {
        Vector3 posToSpawn = node.turretAnchor.position;
        if(pathWaypointsContainer) {
            int waypointsCount = pathWaypointsContainer.transform.childCount;
            GameObject newWaypoint = new GameObject("PathWaypoint_" + waypointsCount.ToString());
            newWaypoint.transform.position = posToSpawn;
            newWaypoint.transform.parent = pathWaypointsContainer.transform;
        }
        else {
            Debug.LogError("PathWaypointsContainer hasn't been found!");
        }

    }

    string CheckBorderNameByPosition(Vector2Int posOnBorder) {
        int borderArraySize = mapConfiguration.mapSize.x-1;
        string borderName;

        if(posOnBorder.x == 0) {
            borderName = "Left";
        }
        else if(posOnBorder.x == borderArraySize) {
            borderName = "Right";
        }
        else if(posOnBorder.y == 0) {
            borderName = "Top";
        }
        else if(posOnBorder.y == borderArraySize) {
            borderName = "Bottom";
        }
        else {
            borderName = "Error";
            Debug.LogError("Border unknown!");
        }

        return borderName;
    }

    [System.Serializable]
    public class MapConfiguration
    {
        public float positionDelta = 2.5f;
        public Vector2Int mapSize = new Vector2Int(10, 10);
        public int nbOfTurrets = 10;
    }
}
