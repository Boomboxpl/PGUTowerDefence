﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class MapNode : MonoBehaviour
{
    public Transform turretAnchor;
    public TurretShooter turretShooter { get; private set; }
    [HideInInspector] public Vector2Int positon;

    public void SetTurret(TurretShooter turret)
    {
        turretShooter = turret;
        turret.transform.SetParent(transform);
        turret.transform.position = turretAnchor.position;
    }

}
