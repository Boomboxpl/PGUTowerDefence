﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatusInfo : MonoBehaviour
{
    private Text healthValueText;
    private Text moneyValueText;


    // Start is called before the first frame update
    void Start()
    {
        InitializeComponents();
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePlayerInfo();
    }

    private void InitializeComponents(){
        AssignTextVariables();
    }

    private void UpdatePlayerInfo() {
        GameManager gM = GameManager.Instance;
        int playerLifePercentage = (int)Mathf.Round(gM.gameScore.life * 100);
        int playerMoney = gM.gameScore.money;
        healthValueText.text = playerLifePercentage.ToString() + " %";
        moneyValueText.text = playerMoney.ToString() + " $";

        if(gM.gameScore.life <= 0) {
            gameObject.SetActive(false);
        }
    }

    private void AssignTextVariables() {
        Text[] tmpTexts = transform.GetComponentsInChildren<Text>();
        bool[] isFound = new bool[2] {false, false};
        foreach (Text text in tmpTexts)
        {
            string gObjName = text.transform.gameObject.name;
            if(!isFound[0] && gObjName == "Health_value") {
                healthValueText = text;
                isFound[0] = true;
            }
            if(!isFound[1] && gObjName == "Money_value") {
                moneyValueText = text;
                isFound[1] = true;
            }
            if(isFound[0] && isFound[1]){
                break;
            }
        }
        if(!isFound[0] || !isFound[1]){
            Debug.LogError("Health and money value texts wasn't found!");
            return;
        }
    }
}
