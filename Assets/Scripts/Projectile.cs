﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    const float stopThreshold = 0.05f;

    public float velocity = 1.5f;
    public float damage = 0.1f;

    public Vector3 Target
    {
        get => target;
        set
        {
            target = value;
            isRunning = true;
        }
    }

    Vector3 target;
    bool isRunning;

    void Awake()
    {
        GameManager.Instance.gameScore.noLife += OnPlayerNoLife;
    }

    void Update()
    {
        if (isRunning)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, velocity * Time.deltaTime);

            if((transform.position - target).magnitude < stopThreshold)
            {
                Destroy(gameObject);
            }
        }
    }

    void OnDestroy()
    {
        if(GameManager.Instance != null)
        {
            GameManager.Instance.gameScore.noLife -= OnPlayerNoLife;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Enemy enemy = other.gameObject.GetComponent<Enemy>();
        if (enemy != null)
        {
            enemy.Hit(damage);
            Destroy(gameObject);
        }
    }

    void OnPlayerNoLife()
    {
        Destroy(gameObject);
    }
}
