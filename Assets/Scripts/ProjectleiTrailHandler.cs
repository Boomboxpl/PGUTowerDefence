﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectleiTrailHandler : MonoBehaviour
{
    public float hueSpread = 20f;
    public float animationDuration = 1f;

    Material material;
    TrailRenderer trail;

    float elapsedTime;

    void Awake()
    {
        material = transform.parent.GetComponent<MeshRenderer>().material;
        trail = GetComponent<TrailRenderer>();
        elapsedTime = 0f;
    }

    void Update()
    {
        elapsedTime += Time.deltaTime;
        if(elapsedTime > animationDuration)
        {
            elapsedTime = 0f;
        }
        float cosTime = (elapsedTime / animationDuration) * 2 * Mathf.PI;
        float hueDelta = Mathf.Cos(cosTime) * hueSpread;
        hueDelta /= 360f;

        float h, s, v;
        Color.RGBToHSV(material.color, out h, out s, out v);
        h += hueDelta;
        if (h > 1)
        {
            h -= 1;
        }
        if(h < 0)
        {
            h += 1;
        }

        Color color = Color.HSVToRGB(h, s, v);
        trail.startColor = color;
        trail.endColor = color;
    }
}
