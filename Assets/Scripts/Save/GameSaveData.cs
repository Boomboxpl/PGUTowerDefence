﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSaveData
{
    public MapManager.MapConfiguration mapConfiguration;
    public List<TurretShooter.TurretConfiguration> turrets;
    public GameScore gameScore;

    public void PollData()
    {
        mapConfiguration = GameManager.Instance.mapManager.mapConfiguration;
        turrets = GameManager.Instance.mapManager.GetTurretsConfigruations();
        gameScore = GameManager.Instance.gameScore;
    }

    public void ApplyData()
    {
    }
}
