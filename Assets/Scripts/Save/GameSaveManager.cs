﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class GameSaveManager : MonoBehaviour
{
    public void SaveGame(string fileName = "save.json")
    {
        GameSaveData data = new GameSaveData();
        data.PollData();
        Debug.Log("Games saved");
        File.WriteAllText($"{Application.persistentDataPath}/{fileName}", JsonUtility.ToJson(data));
    }
}
