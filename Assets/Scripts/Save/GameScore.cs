﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class GameScore
{
    public Action noLife;

    public bool isAlive => _life > 0f;
    public float life
    {
        get => _life;
        set
        {
            if(value <= 0f)
            {
                noLife?.Invoke();
            }
            _life = Mathf.Clamp01(value);
        }
    }
    public int waveCount = 0;
    public int money = 1000;

    [SerializeField] float _life = 1f;

    public GameScore()
    {
        life = 1f;
        waveCount = 0;
        money = 1000;
    }
}
