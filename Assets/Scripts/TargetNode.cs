﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetNode : MonoBehaviour
{
    private Renderer rend;
    public Color selectedColor;
    private Color startColor;


    void Start()
    {
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
    }

    public void Select()
    {
        rend.material.color = selectedColor;
    }

    public void Unselect()
    {
        rend.material.color = startColor;
    }
}
