﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class TargetSelector : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Action clickSelected;

    public Camera camera;
    private MapNode selected;
    public TurretShooter[] turretTemplates;

    public GameObject selectionIndicator;
    public TurretShooter selectedTurret { get; private set; }

    public int currentTurretSelected { get; private set; }

    bool isPointerOver;

    void Awake()
    {
        selectionIndicator.SetActive(false);
        currentTurretSelected = 1;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isPointerOver = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isPointerOver = false;
    }

    void Update()
    {
        if (!GameManager.Instance.isInEdit)
        {
            selected?.GetComponent<TargetNode>().Unselect();
            if(selectedTurret != null)
            {
                selectedTurret = null;
                clickSelected?.Invoke();
            }
            selectionIndicator.SetActive(false);
            return;
        }

        CheckInput();
        if (!isPointerOver)
        {
            return;
        }
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);

        if (selected)
        {
            selected.GetComponent<TargetNode>().Unselect();
            selected = null;
        }

        if (Physics.Raycast(ray, out RaycastHit hitInfo))
        {
            Debug.DrawLine(ray.origin, hitInfo.point);
            MapNode pointedNode = hitInfo.collider.gameObject.GetComponentInParent<MapNode>();
            bool isThisAPath = GameManager.Instance.mapManager.pathNodes.Contains(pointedNode);
            selected = isThisAPath ? null : pointedNode;
        }

        if (selected)
            selected.GetComponent<TargetNode>()?.Select();

        if (Input.GetMouseButtonDown(0))
        {
            UnselectTurret();
            if (selected)
            {
                if (selected.turretShooter == null)
                {
                    PlaceTurret();
                }
                else
                {
                    selectedTurret = selected.turretShooter;
                    selectionIndicator.SetActive(true);
                    selectionIndicator.transform.position = selectedTurret.transform.position + new Vector3(0, 2f, 0);
                }
                clickSelected?.Invoke();
            }
        }
    }

    public void UnselectTurret()
    {
        selectionIndicator.SetActive(false);
        selectedTurret = null;
        clickSelected?.Invoke();
    }

    void CheckInput() {
        if(Input.GetKeyDown(KeyCode.Alpha1)) {
            currentTurretSelected = 1;
        }
        if(Input.GetKeyDown(KeyCode.Alpha2)) {
            currentTurretSelected = 2;
        }
        if(Input.GetKeyDown(KeyCode.Alpha3)) {
            currentTurretSelected = 3;
        }
        if(Input.GetKeyDown(KeyCode.Alpha4)) {
            currentTurretSelected = 4;
        }
        if(Input.GetKeyDown(KeyCode.Alpha5)) {
            currentTurretSelected = 5;
        }
    }

    void PlaceTurret()
    {
        
        
        if (selected != null && selected.turretShooter == null)
        {
            int turretId = currentTurretSelected-1;
            GameManager gM = GameManager.Instance;
            int turretPrice = turretTemplates[turretId].configuration.price;

            bool isAffordable = gM.gameScore.money >= turretPrice ? true : false;
            if (isAffordable) {
                selected.SetTurret(Instantiate(turretTemplates[turretId]));
                selected.turretShooter.PrepareConfigurationData(turretTemplates[turretId].name, selected.positon);
                gM.gameScore.money -= turretPrice;
            }

        }
    }

    public void ChangeTurretSelected(int buttonID) {
        currentTurretSelected = buttonID;
    }
}
