﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSimulation : MonoBehaviour
{
    public float simulationInterval = 1f;
    public float healthLost = 0.1f;
    public int moneyGain = 300;

    void Start()
    {
        StartCoroutine(SimulationCoroutine());
    }

    IEnumerator SimulationCoroutine()
    {
        while (true)
        {
            if (GameManager.Instance.isInEdit)
            {
                yield return new WaitWhile(() => GameManager.Instance.isInEdit);
            }
            yield return new WaitForSeconds(simulationInterval);
            if (!GameManager.Instance.isInEdit)
            {
                Debug.Log("Simulation apply");
                GameManager.Instance.gameScore.life -= healthLost;
                GameManager.Instance.gameScore.money += moneyGain;
            }
        }
    }
}
