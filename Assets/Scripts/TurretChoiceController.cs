﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurretChoiceController : MonoBehaviour
{
    [SerializeField] private List<Button> turretButtons = null;
    [SerializeField] private TargetSelector tSelectorRef = null;

    private int curTurretSelected = 0;
    private bool[] turretsInteractability;
    private bool[] turretsInteractabilitySave;
    private bool wasSwitchedOff = false;


    private void Awake() {
    }

    // Start is called before the first frame update
    void Start()
    {
        CheckCompletness();
        InitializeComponents();
    }

    // Update is called once per frame
    void Update()
    {
        CheckGameState();
        if(!wasSwitchedOff) {
            CheckSelectedTurret();
            CheckAffordableTurrets();  
        }        
    }

    private void CheckGameState() {
        if (GameManager.Instance.isInEdit == false) {
            wasSwitchedOff = true;
            UpdateButtonHighlight(curTurretSelected);
            SwitchAllButtonsOff();
            
        }
        else if (wasSwitchedOff && GameManager.Instance.isInEdit == true) {
            wasSwitchedOff = false;
            SwitchToPreviousState();            
        }
    }
    private void CheckCompletness() {
        if(turretButtons == null) {
            Debug.LogError("There is no buttons assigned!");
        }
        if(tSelectorRef == null) {
            Debug.LogError("TargetSelector is not assigned!");
        }
    }

    private void CheckSelectedTurret() {
        int selectedTurret = tSelectorRef.currentTurretSelected;
        if(curTurretSelected != selectedTurret) {
            int arrayID = selectedTurret - 1;
            UpdateButtonHighlight(arrayID);
            curTurretSelected = selectedTurret;
        }
    }

    private void CheckAffordableTurrets() {
        int money = GameManager.Instance.gameScore.money;
        for(int i=0; i < tSelectorRef.turretTemplates.Length; i++) {
            if(money < tSelectorRef.turretTemplates[i].configuration.price) {
                turretsInteractability[i] = false;
                turretButtons[i].interactable = false;
            }
            else if (!turretsInteractability[i]) {
                turretsInteractability[i] = true;
                turretButtons[i].interactable = true;
            }            
        }
    }

    private void SwitchAllButtonsOff() {
        turretsInteractabilitySave = turretsInteractability;
        for(int i=0; i < turretsInteractability.Length; i++) {
            turretsInteractability[i] = false;
            turretButtons[i].interactable = false;
        }
    }

    private void SwitchToPreviousState() {
        turretsInteractability = turretsInteractabilitySave;
        for(int i=0; i < turretsInteractability.Length; i++) {
            if(turretsInteractability[i]) {
                turretButtons[i].interactable = true;
            }
            else {
                turretButtons[i].interactable = true;
            } 
        }
    }

    private void UpdateButtonHighlight(int buttonId) {
        if (wasSwitchedOff) {
            turretButtons[curTurretSelected-1].image.color = Color.white;
        }
        else {
            //Unselect curTurretSelected
            if(curTurretSelected != 0)
                turretButtons[curTurretSelected-1].image.color = Color.white;

            //Select tSelectorRef.currentTurretSelected;
            turretButtons[buttonId].image.color = Color.yellow;
        }
    }

    private void InitializeComponents() {
        SetTurretPrices();
        InitializeStartingValues();
    }

    private void InitializeStartingValues() {
        curTurretSelected = tSelectorRef.currentTurretSelected;
        UpdateButtonHighlight(curTurretSelected-1);
        turretsInteractability = new bool[tSelectorRef.turretTemplates.Length];
        turretsInteractabilitySave = new bool[tSelectorRef.turretTemplates.Length];
        for (int i=0; i < turretsInteractability.Length; i++) {
            turretsInteractability[i] = true;
        }
        Debug.Log("turretsInteractability.Length: " + turretsInteractability.Length);
    }

    private void SetTurretPrices() {
        int nbOfButtons = turretButtons.Count;
        if (nbOfButtons != tSelectorRef.turretTemplates.Length) {
            Debug.LogError("Not equal number of turrets with number of turret buttons!");
            return;
        }
        for (int i=0; i < nbOfButtons; i++) {
            Button curButton = turretButtons[i];
            Text[] tmpTexts = curButton.transform.GetComponentsInChildren<Text>();
            Text priceButton = null;
            foreach (Text text in tmpTexts)
            {
                if(text.transform.gameObject.name == "Price") {
                    priceButton = text;
                    break;
                }
            }
            if(priceButton == null) {
                Debug.LogError("Price text of a button wasn't found!");
                return;
            }
            priceButton.text = "$" + tSelectorRef.turretTemplates[i].configuration.price.ToString();
        }
    }

    public void ButtonClick(int buttonID) {
        tSelectorRef.ChangeTurretSelected(buttonID);
    }
}
