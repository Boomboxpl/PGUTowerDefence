﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class TurretShooter : MonoBehaviour
{
    const string pathToTurrets = "Turrets/";
    const string pathToProjectiles = "Projectiles/";

    public TurretConfiguration configuration;

    public Projectile projectile;

    public Enemy targetEnemy;

    void OnEnable()
    {
        StartCoroutine(ShootCoroutine());
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, configuration.maxShootDistance);
    }

    public void PrepareConfigurationData(string prefabName, Vector2Int position)
    {
        configuration.turretPrefabPath = pathToTurrets + prefabName;
        configuration.projectilePrefabPath = pathToProjectiles + projectile.name;
        configuration.position = position;
    }

    Enemy TryGetEnemy()
    {
        if(!(Enemy.spawnedEnemies != null && Enemy.spawnedEnemies.Count > 0)) 
        {
            return null;
        }
        float minDistance = Enemy.spawnedEnemies.Min(enemy => (enemy.transform.position - transform.position).magnitude);
        if(minDistance > configuration.maxShootDistance)
        {
            return null;
        }
        return Enemy.spawnedEnemies.Where(enemy => (enemy.transform.position - transform.position).magnitude <= minDistance + Mathf.Epsilon).FirstOrDefault();
    }

    bool TryShoot(Enemy target)
    {
        if(target != null)
        {
            Projectile newProjectile = Instantiate(projectile);
            newProjectile.damage *= configuration.damageScaler;
            newProjectile.transform.position = transform.position;
            newProjectile.Target = target.transform.position;
            return true;
        }
        return false;
    }

    IEnumerator ShootCoroutine()
    {
        while (true)
        {
            if (TryShoot(targetEnemy))
            {
                yield return new WaitForSeconds(configuration.waitTimeAfterShoot);
            }
            else
            {
                yield return null;
            }
        }
    }

    [Serializable]
    public class TurretConfiguration
    {
        public float waitTimeAfterShoot = 1f;
        public float maxShootDistance = 1f;
        public float damageScaler = 1f;
        public int price = 100;
        [HideInInspector] public float scaleScaler = 1f;
        [HideInInspector] public int upgradeLevel = 0;
        [HideInInspector] public Vector2Int position;
        [HideInInspector] public string turretPrefabPath;
        [HideInInspector] public string projectilePrefabPath;
    }

    public void Update() {
        targetEnemy = TryGetEnemy();
        Vector3 toRotate = new Vector3(targetEnemy.transform.position.x, 0.0f, targetEnemy.transform.position.z);
        this.transform.LookAt(toRotate);
        this.transform.Rotate(0.0f, 90.0f, 0.0f);
    }
}
