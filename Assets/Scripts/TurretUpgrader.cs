﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurretUpgrader : MonoBehaviour
{
    const string IsShownAnimator = "IsShown";
    const string turretDescriptionFormat = "Level: {0}/{1}\nDamage: {2}";
    const string upgradeFormat = "Upgrade({0})";

    public int maxLevel = 3;
    public int upgradeCostPerLevel = 200;

    public Text turretDescription;
    public Text upgradeText;
    public Button unselectButton;
    public Button upgradeButton;

    TargetSelector targetSelector;
    Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
        targetSelector = FindObjectOfType<TargetSelector>();

        targetSelector.clickSelected += ShowHide;

        unselectButton.onClick.AddListener(targetSelector.UnselectTurret);
        upgradeButton.onClick.AddListener(UpgradeTurret);

        ShowHide();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            UpgradeTurret();
        }
    }

    void OnDestroy()
    {
        targetSelector.clickSelected -= ShowHide;
    }

    void ShowHide()
    {
        UpdateUI();
        animator.SetBool(IsShownAnimator, targetSelector.selectedTurret != null);
    }

    void UpgradeTurret()
    {
        TurretShooter turret = targetSelector.selectedTurret;
        if (CanUpgrade(turret))
        {
            GameManager.Instance.gameScore.money -= GetUpgradeCost(turret);
            turret.configuration.damageScaler *= 2;
            turret.configuration.upgradeLevel++;
            Debug.Log("Turret upgraded. Damage scaler now: " + turret.configuration.damageScaler);
        }
        UpdateUI();
    }

    void UpdateUI()
    {
        TurretShooter turret = targetSelector.selectedTurret;
        if (turret != null)
        {
            turretDescription.text = string.Format(turretDescriptionFormat,
                turret.configuration.upgradeLevel,
                maxLevel,
                turret.projectile.damage * turret.configuration.damageScaler
                );
            upgradeText.text = IsMaxLevel(turret) ? "Max" : string.Format(upgradeFormat, GetUpgradeCost(turret));
            upgradeButton.interactable = CanUpgrade(turret);
        }
    }

    bool CanUpgrade(TurretShooter turret)
    {
        return turret != null && !IsMaxLevel(turret) && GameManager.Instance.gameScore.money >= GetUpgradeCost(turret);
    }

    bool IsMaxLevel(TurretShooter turret) => turret.configuration.upgradeLevel >= maxLevel;

    int GetUpgradeCost(TurretShooter turret) => upgradeCostPerLevel * (turret.configuration.upgradeLevel + 1);
}
