﻿Shader "Custom/PostShader" {
    Properties {
        _MainTex ("Render image", 2D) = "white" {}
        _desaturation ("Desaturation level", Range (0, 1)) = 0
    }
    SubShader {
        Pass {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag

            #include "UnityCG.cginc"

            uniform sampler2D _MainTex;
            uniform float _desaturation;

            float4 frag(v2f_img i) : COLOR {
                float4 c = tex2D(_MainTex, i.uv);
                
                float f = c.r * 0.3 + c.g * 0.59 + c.b * 0.11;
                float3 gray = float3(f, f, f); 
                
                float4 result = c;
                result.rgb = lerp(c.rgb, gray, _desaturation);
                return result;
            }
            ENDCG
        }
    }
}