﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowShader : MonoBehaviour
{
    public float intensityMax;
    public float intensityMin;

    public Material material;

    float intensity;

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        intensity = Mathf.InverseLerp(intensityMin, intensityMax, GameManager.Instance.gameScore.life);
        if (!Mathf.Approximately(intensity, 1f))
        {
            intensity = 1 - intensity;
            Grayscale(source, destination);
            return;
        }

        Graphics.Blit(source, destination);
    }

    void Grayscale(RenderTexture source, RenderTexture destination)
    {
        material.SetFloat("_desaturation", intensity);
        Graphics.Blit(source, destination, material);
    }
}
